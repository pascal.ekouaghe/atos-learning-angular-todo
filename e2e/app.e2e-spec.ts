import { AtosLearningAngularTodoPage } from './app.po';

describe('atos-learning-angular-todo App', () => {
  let page: AtosLearningAngularTodoPage;

  beforeEach(() => {
    page = new AtosLearningAngularTodoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
