import {Component} from "@angular/core";

@Component({
  selector: 'todo-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private todos: Todo[] = [
    new Todo("Learn TypeScript"),
    new Todo("Learn Angular"),
  ];

  public hasItems(): boolean {
    // TODO This should return if we actually have some items
    return false;
  }
}

class Todo {
  constructor(public title: string) {
  }
}
